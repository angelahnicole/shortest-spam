/*******************************************************
 * Vertex.java
 * 
 * Angela Gross
 * 
 * Extremely simple class that has a tuple of x and y
 * registers and a SPAM instruction for our SPAM program-
 * Has getters and tuple equality check. Is called a 
 * vertex since we use these tuples in BFS to create
 * graphs of possible SPAM instructions.
 * 
 *******************************************************/

public class Vertex 
{
	//////////////////////////////////////////////////////////////////
	
	// declaring ATTRIBUTES
	protected int x = 0;
	protected int y = 0;
	protected String SPAMinstr = "";
	
	//////////////////////////////////////////////////////////////////
	
	// CONSTRUCTOR
	public Vertex (int myX, int myY, String instr)
	{
		x = myX;
		y = myY;
		SPAMinstr = instr;
	}
	
	//////////////////////////////////////////////////////////////////
	
	//--------------------------------------------------------
	// Getter METHODS
	//--------------------------------------------------------
	// I realize that I could've just used "v.x", but it just
	// made it more readable for me to use my own getter 
	// methods instead.
	// --------------------------------------------------------
	
	public int getX(){return x;}
	
	public int getY(){return y;}
	
	public String getSPAMinstr(){return SPAMinstr;}
	
	//////////////////////////////////////////////////////////////////
	
	// Does this tuple have the same x and y values?
	public boolean isVertEqual(Vertex thatVert)
	{	
		return ((thatVert.getX() == x) && (thatVert.getY() == y));
	}
	
}
