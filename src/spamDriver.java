/*******************************************************
 * spamDriver.java
 * 
 * Angela Gross
 * 
 * Uses shortestSpam to find the fewest number of 
 * instructions of the SPAM program, (which is described
 * in detail in shortestSpam), given two registers, x and
 * y, and a value of z. 
 * 
 *  Reads in a file "proj2.dat" in the current working 
 *  directory, with integer values of x, y, and z, 
 *  respectively.
 *  
 *  If an instruction set exists, then prints out the 
 *  SPAM instructions and exits. If an instruction set 
 *  doesn't exist, then prints "Impossible" and exits.
 *  
 *  Note: x, y, and z are ASSUMED to be integers less
 *  than 1000. 
 * 
 *******************************************************/

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class spamDriver 
{
	//////////////////////////////////////////////////////////////////
	
	// Main METHOD
	public static void main(String[] args) throws FileNotFoundException 
	{
		File myFile = new File("./proj2.dat");
		Vertex myVertex = null;
		shortestSpam myPath = null;
		
		inputFile(myFile, myVertex, myPath);
	}
	
	//////////////////////////////////////////////////////////////////
	
	// -------------------------------------------------
	// inputFile METHOD
	// -------------------------------------------------
	// Takes file, parses it, and creates a Vertex and 
	// shortestSpam object with tokens. ONLY accepts
	// int, a variable amount of spaces, int, a variable
	// amount of spaces, and an int. It *ONLY* reads the
	// first line of input- if inputs are separated by 
	// CRs (or not within range of 1 to 1000, then will 
	// return "Impossible"). Note: It will most likely 
	// break if inputs are delimited by something else.
	// -------------------------------------------------
	
	public static void inputFile(File myFile, 
		Vertex myVertex, shortestSpam myPath) throws FileNotFoundException
	{
		Scanner myScan = new Scanner(myFile);
		int x = 0, y = 0, z = 0;

		String token = myScan.nextLine();
		Scanner myToken = new Scanner(token);
		myToken.useDelimiter("[ ]+");

		while (myToken.hasNext()) 
		{
			x = myToken.nextInt();
			y = myToken.nextInt();
			z = myToken.nextInt();
		}

		if(areValidValues(x, y, z))
		{
			myVertex = new Vertex(x, y, "");
			myPath = new shortestSpam(myVertex, z);
			System.out.println(myPath.findPath());
		}
		else
		{
			System.out.println("Impossible.");
		}
	}
	
	// Bounds Check
	public static boolean areValidValues(int x, int y, int z)
	{
		return ((x > 0 && y > 0 && z > 0) && 
				(x < 1000 && y < 1000 && z < 1000));
	}

}
