/*******************************************************
 * shortestSpam.java
 * 
 * Angela Gross
 * 
 * This computes the shortest path, if it exists, of
 * instructions S, P, A, and M given two registers,
 * x and y, and a desired output, z. Using the 
 * instructions S, P, A, and M, we must find the shortest
 * way to create z from x and y using tuples. This is 
 * done by using Breadth-First Search, i.e. BFS, and
 * printing the result when a value z is found in x.
 * 
 * Here are the possible instructions of this SPAM 
 * language:
 * S: swaps the contents of the two registers
 * P: prints the value in register X
 * A: adds the current contents of the two registers 
 * and puts the result in register X (clobbering what was 
 * there before)
 * M: multiplies the current contents of the two registers
 * and puts the result in register Y (clobbering what was 
 * there before)
 * 
 * I created inside-out objects with bounds of 999 x 999
 * (instead of z by z) on off chance that z can be a value
 * equal to y but less than x. (or something like that)
 * 
 *******************************************************/

import java.util.ArrayList;
import java.util.LinkedList;

public class shortestSpam 
{
	//////////////////////////////////////////////////////////////////
	
	// declaring ATTRIBUTES
	protected Vertex sourceVert;
	protected static int z;
	protected ArrayList<String> pathList = new ArrayList<String>();
	
	// inside-out objects!
	protected String[][] Color;
	protected Vertex[][] Pred;
	
	//////////////////////////////////////////////////////////////////
	
	// CONSTRUCTOR
	public shortestSpam(Vertex myVert, int myOut)
	{
		sourceVert = myVert;
		z = myOut;
		Color = new String[1000][1000]; 
		Pred = new Vertex[1000][1000]; 
	}
	
	//////////////////////////////////////////////////////////////////
	
	// -------------------------------------------------------------
	// findPath() METHOD
	// -------------------------------------------------------------
	// This is basically BFS with the use of "inside-out" objects-
	// the neat thing about using these is that the "impossible"
	// exception is extremely easy- "if you can't fit in the 
	// dimensions of my array, then you are not considered."
	// I was having a lot of problems without the use of these
	// objects, and after I converted my code to use these, it all
	// worked like a charm. (in retrospect, I think it was because
	// I was adding neighbors when I shouldn't have been, but I think
	// I like this way much better anyway)
	//
	// Notes: 
	// - Since all neighbors of sourceVert do not check sourceVert
	// if it has z, I have a quick check before BFS begins. Also, we
	// know that we empty out the queue and haven't found a z value,
	// then, of course, it is impossible to create a SPAM program for
	// those values of x, y, and z.
	// - Since I allow for tuples in the range of 999 X 999, I only
	// want to add on the queue values of x OR y that are less than 
	// or equal to z.
	//
	// Everything else is standard procedure!
	// -------------------------------------------------------------
	
	public String findPath() 
	{
		if(isZPath(sourceVert))
			return printPath(sourceVert);
		
		vertQueue myQ = new vertQueue();
		Vertex u = null;
		Vertex v = null;
		ArrayList<Vertex> Neighbors = null;
		
		for(int i=0; i<Color.length; i++)
		{
			for(int j=0; j<Color.length; j++)
			{
				Color[i][j] = "White";
				Pred[i][j] = null;
			}
		}
		
		Color[sourceVert.getX()][sourceVert.getY()] = "Gray";
		Pred[sourceVert.getX()][sourceVert.getY()] = null;

		myQ.enqueue(sourceVert);

		while(!myQ.isEmpty())
		{
			u = myQ.dequeue();
			Neighbors = getNeighbors(u);

			for(int i=0; i < Neighbors.size(); i++)
			{
				v = Neighbors.get(i);
				try
				{
					if (Color[v.getX()][v.getY()].equalsIgnoreCase("White"))
					{
						Color[v.getX()][v.getY()] = "Gray";
						Pred[v.getX()][v.getY()] = u;

						if (isZPath(v))
							return printPath(v);

						if(inZRange(v))
							myQ.enqueue(v);
					}
				}
				catch (ArrayIndexOutOfBoundsException e) {}
			}

			Color[u.getX()][u.getY()] = "Black";
		}

		return "Impossible.";
	}
	
	//////////////////////////////////////////////////////////////////
	
	// ------------------------------------------------
	// BFS HELPER METHODS
	// ------------------------------------------------
	// getNeighbors(), of course, creates an adjacency 
	// list containing all of the possible SPAM 
	// instructions given x and y.
	//
	// isZPath() is just a pretty way to ask "is v.x
	// equal to z?"
	//
	// inZRange() is just a bounds check- since I check 
	// if x == z before I call this I don't need to 
	// check for that, but I need to check for y == x.
	// ------------------------------------------------
	
	public ArrayList<Vertex> getNeighbors(Vertex u)
	{
		int ux = u.getX();
		int uy = u.getY();
		ArrayList<Vertex> Neighbors = new ArrayList<Vertex>(3);
		
		Neighbors.add(new Vertex(uy, ux, "S"));
		Neighbors.add(new Vertex(ux+uy, uy, "A"));
		Neighbors.add(new Vertex(ux, uy*ux, "M"));
		
		return Neighbors;
	}
	
	public boolean isZPath(Vertex v)
	{
		return v.getX() == z;
	}
	
	public boolean inZRange(Vertex v)
	{
		return (v.getX() < z || v.getX() <= z);
	}
	
	//////////////////////////////////////////////////////////////////
	
	// ------------------------------------------------
	// METHODS THAT PRINT THE DESIRED PATH
	// ------------------------------------------------
	// Note: I realized that, when I start from the
	// desired vertex, I am printing out the SPAM
	// instructions backwards- thus why I have an 
	// ArrayList.
	//
	// printPath() uses predecessor values from the
	// inside-out object in order to print its
	// shortest path, as created by the BFS.
	//
	// printList() just takes the ArrayList and prints
	// it in reverse- nothing fancy.
	// ------------------------------------------------
	

	public String printPath(Vertex v)
	{
		pathList.add("P");
		
		if(!v.isVertEqual(sourceVert))
		{
			do
			{
				pathList.add(v.getSPAMinstr());
				v = Pred[v.getX()][v.getY()];
				
			}while(!v.isVertEqual(sourceVert));
		}
		
		return printList();
	}
	
	public String printList()
	{
		String path = "";
		
		for(int i=pathList.size()-1; i >= 0; i--)
		{
			path = path + pathList.get(i);
		}
		
		return path;
	}
	
	//////////////////////////////////////////////////////////////////
}
